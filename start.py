# -*- coding: utf-8 -*-
# author:liucong  user.email=1335706545@qq.com
from core import index


def start():
    index.main()


# 从根目录启动，确保相对路径调用正常
if __name__ == '__main__':
    start()