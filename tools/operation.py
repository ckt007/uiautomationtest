import importlib
from urllib import request
import urllib.parse
from airtest.core.api import *
from tools.log import logger
import subprocess
from  core import MultiAdb
from tools.Screencap import *


"""
唤起飞猪App
"""
def start_fliggy():
    deviceType=MultiAdb.MultiAdb().detect_device_type()#
    if deviceType=='Android':
        try:
            packageName = 'com.taobao.trip'
            shell('am start -n {}/com.alipay.mobile.quinox.LauncherActivity'.format(packageName))
        except Exception as e:
            logger.info("start fliggy suffer from exception,the reason is:{}".format(e))
    elif deviceType=='IOS':
        try:
            bundle_id = 'com.taobao.travel'
            start_app(bundle_id)
        except Exception as e:
            logger.info("start fliggy suffer from exception,the reason is:{}".format(e))
    try:
        GetScreenbyADBCap(time.time(),deviceType,'jietu')
    except Exception as e:
        print(e)
"""
结束飞猪App
"""
def stop_fliggy():
    deviceType = MultiAdb.MultiAdb().detect_device_type()  #
    if deviceType == 'Android':
        packageName='com.taobao.trip'
        stop_app(packageName)
    elif deviceType == 'IOS':
        bundle_id = 'com.taobao.travel'
        stop_app(bundle_id)
